package ol;

import java.math.BigDecimal;

public class Math {
    //Devuelve el n t�rmino de la serie Fibonacci
    //La sucesi�n comienza con los n�meros 0 y 1, y a partir de estos, �cada t�rmino es la suma de los dos anteriores�
    public static double getFibonacci(int n) {
        double s=1,u=0,p;
        int i;
        switch (n) {
            case 0:
                s=0;
                break;
            case 1:
            case 2:
                s=1;
                break;
            default:
                for(i=3;i<=n;i++)
                {
                    p=u;
                    u=s;
                    s=s+p;
                }
                break;
        }
        return s;
    }
    //Devuelve la serie Fibonacci hasta el n�mero n
    //La sucesi�n comienza con los n�meros 0 y 1, y a partir de estos, �cada t�rmino es la suma de los dos anteriores�
    public static String getFibonacciSeries(int n) {
        double s=1,u=0,p;
        String series="";
        int i;
        switch(n){
            case 0:
                series="0";
                break;
            case 1:
                series="0";
                break;
            case 2:
                series="0, 1";
                break; 
            default:
                series="0, 1";
                 for(i=3;i<=n;i++)
                {
                    p=u;
                    u=s;
                    s=s+p;
                    series+=", "  + getNumberToString(s);
                }               
                break;
        }
        return series;
    }	    
    //Devuel el factorial de un entero positivo
    //el factorial de n o n factorial se define en principio como el producto de todos los n�meros enteros positivos desde 1 hasta n 
    public static double getFactorial(int n){
        int i;
        double f=1;
        if (n>1)
            for (i=2;i<=n;i++)
                f*=i;
        return f;
    }    
    //En matem�ticas, un n�mero primo es un n�mero natural mayor que 1 que tiene �nicamente dos divisores distintos: �l mismo y el 1.
    public static boolean isPrimeNumber(int n) {
        int c=0,i=1;
        if (n>2)
          while ((i<=n)&&(c<3)){
               if((n%i)==0)
                   c++;
               i++;
           }
       return !(c>2);
    }    
    public static boolean isNumeric(String s) {  
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");  
    }      
    public static String getNumberToString(Double number){
        BigDecimal d = new BigDecimal(number);
        long a = d.longValue();
        BigDecimal b = d.remainder(BigDecimal.ONE);
        if (b==BigDecimal.ZERO) {
            return String.valueOf(a);
        } else {
            return String.valueOf(number);
        }       
    }
}

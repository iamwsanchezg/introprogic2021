package ol;

public class Convert {
    public static String decimalToBinary(int input) {
    	String temp = "", output = "";
    	char [] items;
    	int dividend, divisor, quotient, remainder;
    	dividend = input;
    	divisor = 2;
    	quotient = dividend / divisor;
    	remainder = dividend % divisor;
    	temp = String.valueOf(remainder);
    	
    	while (quotient>1) {
    		dividend = quotient;
        	quotient = dividend / divisor;
        	remainder = dividend % divisor;
        	temp += String.valueOf(remainder);
    	}
    	
    	temp += String.valueOf(quotient);
    	
    	items = temp.toCharArray();
    	
    	for(int i = items.length - 1; i>=0; i--) {
    		output+=items[i];
    	}
    	
    	return output;
    }
    
    public static int binaryToDecimal(String input) {
    	String temp = "";
    	int output = 0;
    	char [] items;
    	items = input.toCharArray();
    	int y = 0;
    	for(int i = items.length - 1; i>=0; i--) {
    		int x = Character.getNumericValue(items[i]);
    		if(x==1) {
        		output+= java.lang.Math.pow(2, y);
    		}
    		y++;
    	} 	
    	
    	return output;
    }
}

package ui;

import java.util.Scanner;

public class Demo {
	public void show() {
		int a, b, c;
		float d;
		Scanner scan = new Scanner(System.in);
		System.out.println("Indica a: ");
		a = scan.nextInt();
		System.out.println("Indica b: ");
		b = scan.nextInt();
		c = a + b;
		d = (float)a/b;
		System.out.format("a + b = %d\n", c);
		System.out.format("a / b = %f", d);
	}
}

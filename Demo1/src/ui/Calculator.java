package ui;

import java.util.Scanner;

import ol.Convert;
import ol.Math;

public class Calculator {
	public void sum() {
		double a, b, c;
		Scanner scan = new Scanner(System.in);
		System.out.println("Indica a: ");
		a = scan.nextDouble();
		System.out.println("Indica b: ");
		b = scan.nextDouble();
		c = a + b;
		System.out.format("a + b = %f\n", c);
	}
	public void getFibonacci() {
		int a;
		Scanner scan = new Scanner(System.in);
		System.out.println("Indica el t�rmino que deseas saber: ");
		a = scan.nextInt();
		System.out.format("fib(%d)= %s", a, Math.getNumberToString(Math.getFibonacci(a)));
	}
	
	public void getFibonacciSeries() {
		int a;
		Scanner scan = new Scanner(System.in);
		System.out.println("Indica hasta que t�rmino obtener la serie: ");
		a = scan.nextInt();
		System.out.format("fib(%d)= %s", a, Math.getFibonacciSeries(a));
	}	
	public void getFactorial() {
		int a;
		Scanner scan = new Scanner(System.in);
		System.out.println("Indica el n�mero: ");
		a = scan.nextInt();
		System.out.format("%d!= %s", a, Math.getNumberToString(Math.getFactorial(a)));
	}
	
	public void isPrimeNumber() {
		int a;
		Scanner scan = new Scanner(System.in);
		System.out.println("Indica el n�mero: ");
		a = scan.nextInt();
		System.out.format(Math.isPrimeNumber(a)?"Es primo": "No es primo");
	}
	public void decimalToBinary() {
		int a;
		Scanner scan = new Scanner(System.in);
		System.out.println("Indica el n�mero: ");
		a = scan.nextInt();
		System.out.format("\n %s", Convert.decimalToBinary(a));
	}
	
	public void binaryToDecimal() {
		String a;
		Scanner scan = new Scanner(System.in);
		System.out.println("Indica el n�mero: ");
		a = scan.next();
		System.out.format("\n %d", Convert.binaryToDecimal(a));
	}
}

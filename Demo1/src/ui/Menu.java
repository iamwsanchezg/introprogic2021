package ui;

import java.util.Scanner;

public class Menu {
	Scanner scan = new Scanner(System.in);
	public void show() {	
		while(true) {			
	        System.out.println("\n\n*************************");
	        System.out.println("********OPCIONES*********");
	        System.out.println("1.Suma");
	        System.out.println("2.Determinar si es primo");
	        System.out.println("3.Calcular factorial");
	        System.out.println("4.Obtener en�simo termino fibonacci");
	        System.out.println("5.Obtener sucesi�n fibonacci hasta n");
	        System.out.println("6.Convertir decimal a binario");
	        System.out.println("7.Convertir binario a decimal");
	        System.out.println("8. Salir");
	        System.out.println("*************************\n");
	        executeOption(catchOption());
		}

	}
	
	public byte catchOption() {
		byte option = scan.nextByte();
		while(option<1||option>8) 
		{
	        System.out.println("Por favor, elige una opci�n v�lida.\n");
	        option = scan.nextByte();
		}
		return option;
	}
	
	public void executeOption(byte option) {
		Calculator calc = new Calculator();
		switch(option) {
			case 1:
				calc.sum();
				break;
			case 2:
				calc.isPrimeNumber();
				break;
			case 3:
				calc.getFactorial();
				break;
			case 4:
				calc.getFibonacci();
				break;
			case 5:
				calc.getFibonacciSeries();
				break;
			case 6:
				calc.decimalToBinary();
				break;		
			case 7:
				calc.binaryToDecimal();
				break;						
			case 8:
				exit();
				break;
		}
	}
	
	public void exit() {
		scan.close();
		System.out.println("Aplicaci�n terminada.\n");
		System.exit(0);
	}
}
